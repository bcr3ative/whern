{{ content() }}

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="10000">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            {{ image("img/concert.jpg", "alt": "Concert") }}
            <div class="carousel-caption">
                <h1>Concerts</h1>
                <p>Find out concerts around your town.</p>
            </div>
        </div>
        <div class="item">
            {{ image("img/coffee.jpg", "alt": "Coffee") }}
            <div class="carousel-caption">
                <h1>Hanging Out</h1>
                <p>Hang out with friends or collegues.</p>
            </div>
        </div>
        <div class="item">
            {{ image("img/travel.jpg", "alt": "Travel") }}
            <div class="carousel-caption">
                <h1>Traveling</h1>
                <p>Travel to famous locations or attend events around the world.</p>
            </div>
        </div>
    </div>
</div>
<br>
{{ form("event/search") }}
<div class="row">
    <div class='col-md-4'>
        <div class="form-group">
            <div class='input-group input-group-lg'>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-globe"></span>
                </span>
                {{ form.render('event_city', ['class': 'form-control', 'placeholder': 'City']) }}
            </div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class="form-group">
            <div class='input-group input-group-lg'>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-glass"></span>
                </span>
                {{ form.render('category_id', ['class': 'form-control']) }}
            </div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class="form-group">
            <div class='input-group date input-group-lg'>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                {{ form.render('event_time', ['class': 'form-control', 'id': 'datetimepicker', 'placeholder': 'Date From']) }}
            </div>
        </div>
    </div>
    <div class='col-md-2'>
        {{ submit_button("Search", "class": "btn btn-primary btn-lg") }}
    </div>
</div>
{{ end_form() }}