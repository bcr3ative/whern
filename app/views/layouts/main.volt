<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url(".") }}">Whern</a>    
        </div>
        {{ elements.getMenu() }}
            <!-- <a href="{{ url("./login") }}" style="margin-left:5px;" class="btn btn-primary navbar-btn navbar-right" role="button">Log In</a>
            <a href="{{ url("./signup") }}" class="btn btn-success navbar-btn navbar-right" role="button">Sign Up</a>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url("./profile") }}">Profile Settings</a></li>
                        <li><a href="#">Account Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ url("./home/logout") }}">Logout</a></li>
                  </ul>
                </li>
            </ul>
            <a href="{{ url("./event") }}" class="btn btn-default navbar-btn navbar-right" role="button">Become a Host</a> -->
    </div>
</nav>

<div class="container">
    {{ flash.output() }}
    {{ content() }}
    <hr>
    <footer>
        <p>&copy; Whern 2016</p>
    </footer>
</div>
