{{ content() }}

<h1>Change your password</h1>
{{ form("./security/changePassword") }}
<div class='row'>
	<div class='col-sm-6'>
		<div class="form-group">
			{{ form.label('old_password') }}
			{{ form.render('old_password', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('new_password') }}
			{{ form.render('new_password', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('repeated_password') }}
			{{ form.render('repeated_password', ['class': 'form-control']) }}
		</div>
		<div class="control-group">
			{{ submit_button("Save", "class": "btn btn-primary") }}
			<a href="{{ url("./index/index") }}" class="btn btn-danger" role="button">Cancel</a>
		</div>
	</div>
</div>
{{ end_form() }}