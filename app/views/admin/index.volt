{{ content() }}

<h3>Certification requests</h3>
<div class='row'>
	<div class='col-sm-6'>
		<div class="panel panel-info">
			<div class="panel-heading">Certification requests</div>
			<table class="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Surname</th>
						<th>E-mail</th>
						<th>Nickname</th>
					</tr>
				</thead>
				<tbody>
					{%for user in users%}
						<tr>
							<td>{{user.name}}</td>
							<td>{{user.surname}}</td>
							<td>{{user.email}}</td>
							<td>{{user.nickname}}</td>
							<td><a href="{{ url("./admin/accept?user=" ~user.id_user) }}" class="btn btn-primary" role="button">Accept</a></td>
							<td><a href="{{ url("./admin/reject?user=" ~user.id_user) }}" class="btn btn-danger" role="button">Reject</a></td>
						</tr>
					{%endfor%}
				</tbody>
			</table>
		</div>
	</div>
</div>
<br>

