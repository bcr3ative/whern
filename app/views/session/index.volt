{{ content() }}

<h1>Login</h1>
{{ form("./session/login") }}
<div class='row'>
	<div class='col-sm-6'>
		<div class="form-group">
			{{ form.label('email') }}
			{{ form.render('email', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('password') }}
			{{ form.render('password', ['class': 'form-control']) }}
		</div>
		<div class="control-group">
			{{ submit_button("Log in", "class": "btn btn-primary") }}
		</div>
	</div>
</div>
{{ end_form() }}

<?php echo $this->tag->linkTo("./signup", "Sign Up Here!"); ?>