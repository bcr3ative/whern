{{ content() }}
{{ form("./profile/edit") }}
<div class='row'>
	<div class='col-sm-6'>
		<div class="form-group">
			{{ form.label('email') }}
			{{ form.render('email', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('nickname') }}
			{{ form.render('nickname', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('name') }}
			{{ form.render('name', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('surname') }}
			{{ form.render('surname', ['class': 'form-control']) }}
		</div>
		<div class="control-group">
			{{ submit_button("Save changes", "class": "btn btn-primary", "name": "edit_profile") }}
			<a href="{{ url("./profile/index") }}" class="btn btn-primary" role="button">Cancel</a>
		</div>
	</div>
</div>
{{ end_form() }}