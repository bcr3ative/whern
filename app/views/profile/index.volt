{{ content() }}

<h3>My basic info</h3>
<hr>
<a href="{{ url("profile/edit") }}" class="btn btn-primary" role="button">Edit profile</a>
<a href="{{ url("security/index") }}" class="btn btn-primary" role="button">Change password</a>
<h4>Name: {{user.name}} </h4>
<h4>Surname: {{user.surname}} </h4>
<h4>E-mail: {{user.email}} </h4>
<h4>Nickname: {{user.nickname}} </h4>

<div class='row'>
	<div class="panel panel-info">
		<div class="panel-heading">Visited events</div>
		<table class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Date and time</th>
					<th>City</th>
					<th>Address</th>
					<th>Duration</th>
				</tr>
			</thead>
			<tbody>
				{%for arrival in arrivals%}
					<tr>
						<td>{{ link_to('event/eventInfo/' ~ arrival.id_event, arrival.name) }}</td>
						<td>{{arrival.description}}</td>
						<td>{{arrival.time}}</td>
						<td>{{arrival.city}}</td>
						<td>{{arrival.address}}</td>
						<td>{{arrival.days_num}} days</td>
					</tr>
				{%endfor%}
			</tbody>
		</table>
	</div>
</div>
