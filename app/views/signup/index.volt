{{ content() }}

<h1>Sign up</h1>
{{ form("./signup/register") }}
<div class='row'>
	<div class='col-sm-6'>
		<div class="form-group">
			{{ form.label('email') }}
			{{ form.render('email', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('password') }}
			{{ form.render('password', ['class': 'form-control']) }}
		</div>
		<div class="form-group">
			{{ form.label('nickname') }}
			{{ form.render('nickname', ['class': 'form-control']) }}
		</div>
		<div class="control-group">
			{{ submit_button("Register", "class": "btn btn-primary") }}
		</div>
	</div>
</div>
{{ end_form() }}
