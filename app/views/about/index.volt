{{ content() }}

<h1 align="center">Whern aka When/Where?</h1>

<h3 align="center">Whern is a real-time local discovery event web service. <br>Its primary focus is to provide users location based recommendations for entertaining or exclusive events.</h3>