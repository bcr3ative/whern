{{ content() }}

<div class="row">
	<div class="col-md-6">
		<input type="hidden" id="longitude" value="{{event_info.longitude}}" />
		<input type="hidden" id="latitude" value="{{event_info.latitude}}" />
		<h3>About event</h3>
		<h4>Name: {{ event_info.name }}</h4>
		<h4>Description: {{ event_info.description }}</h4>
		<h4>Date and time: {{ event_info.time }}</h4>
		<h4>Duration: {{ event_info.days_num }}
			{% if event_info.days_num == 1 %}
				day
			{% else %}
				days
			{% endif %}
		</h4>
		<h4>City: {{ event_info.city }}</h4>
		<h4>Address: {{ event_info.address }}</h4>
		{% if arrival == 0 %}
			<a href="{{ url("event/apply/" ~event_info.id_event) }}" class="btn btn-primary" role="button">Apply</a>	
		{% endif %}
		
	</div>
	<div class="col-md-6"><div id="map" style="height:500px;width:100%;"></div></div>
</div>
<h1>Comments</h1>
<hr>
<div class='row'>
	<div class='col-sm-6'>
		{{ form("./event/addComment") }}
			<div class="form-group">
				{{ form.render('id_event', ['class': 'form-control', 'value': event_info.id_event]) }}
			</div>
			<div class="form-group">
				{{ form.render('comment', ['class': 'form-control']) }}<br>
				{{ submit_button("Add comment", "class": "btn btn-success") }}
			</div>
		{{ end_form() }}
	</div>
</div>
<div class="row">
	<div class="panel panel-info">
		<div class="panel-heading">Comments</div>
		<table class="table">
			<thead>
				<tr>
					<th>Author</th>
					<th>Comment</th>
					<th>Date and time</th>
				</tr>
			</thead>
			<tbody>
				{%for comment in comments%}
					<tr>
						<td>{{ comment.nickname }}</td>
						<td>{{ comment.text }}</td>
						<td>{{ comment.time }}</td>
					</tr>
				{%endfor%}
			</tbody>
		</table>
	</div>
</div>