{{ content() }}

{{ form("event/edit/"~event.id_event) }}
	<div class='row'>
		<div class='col-sm-6'>
			<div class="form-group">
				{{ form.label('event_name') }}
				{{ form.render('event_name', ['class': 'form-control', 'value': event.name ]) }}
			</div>
			<div class="form-group">
				{{ form.label('description') }}
				{{ form.render('description', ['class': 'form-control', 'value': event.description ]) }}
			</div>
			<div class="form-group">
				{{ form.label('event_category') }}
				{{ form.render('event_category', ['class': 'form-control' ]) }}
			</div>
			<div class="form-group">
				{{ form.label('city') }}
				{{ form.render('city', ['class': 'form-control', 'value': event.city ]) }}
			</div>
			<div class="form-group">
				{{ form.label('address') }}
				{{ form.render('address', ['class': 'form-control', 'value': event.address ]) }}<br><button id="verifyaddress" type="button" class="btn btn-warning">Retrieve location</button>
			</div>
			<div class="form-group">
				{{ form.render('latitude', ['class': 'form-control', 'placeholder': 'Empty']) }}
			</div>
			<div class="form-group">
				{{ form.render('longitude', ['class': 'form-control', 'placeholder': 'Empty']) }}
			</div>
			<div class="form-group">
				{{ form.label('time') }}
				<div class='input-group date'>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
					{{ form.render('time', ['class': 'form-control', 'id': 'datetimepicker', 'value': event.time ]) }}
				</div>
			</div>
			<div class="form-group">
				{{ form.label('duration') }}
				{{ form.render('duration', ['class': 'form-control', 'value': event.days_num ]) }}
			</div>
			<div class="control-group">
				{{ submit_button("Save changes", "class": "btn btn-success") }}
				<a href="{{ url("./index/index") }}" class="btn btn-default" role="button">Cancel</a>
			</div>
		</div>
	</div>
{{ end_form() }}