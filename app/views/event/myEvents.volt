{{ content() }}

<h1>My events</h1>
<div class='row'>
	<div class="panel panel-info">
		<div class="panel-heading">My hosted events</div>
		<table class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Date and time</th>
					<th>City</th>
					<th>Address</th>
					<th>Duration</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{%for event in events%}
					<tr>
						<td>{{ link_to('event/eventInfo/' ~ event.id_event, event.name) }}</td>
						<td>{{event.description}}</td>
						<td>{{event.time}}</td>
						<td>{{event.city}}</td>
						<td>{{event.address}}</td>
						<td>{{event.days_num}} days</td>
						<td><a href="{{ url("event/edit/" ~event.id_event) }}" class="btn btn-primary" role="button">Edit event</a></td>
						<td><a href="{{ url("event/delete/" ~event.id_event) }}" class="btn btn-primary" role="button">Delete event</a></td>
					</tr>
				{%endfor%}
			</tbody>
		</table>
	</div>
</div>