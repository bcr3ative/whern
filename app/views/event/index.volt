{{ content() }}

{% if certified == 1 %}
	{{ form("event/add") }}
	<div class='row'>
		<div class='col-sm-6'>
			<div class="form-group">
				{{ form.label('event_name') }}
				{{ form.render('event_name', ['class': 'form-control']) }}
			</div>
			<div class="form-group">
				{{ form.label('description') }}
				{{ form.render('description', ['class': 'form-control']) }}
			</div>
			<div class="form-group">
				{{ form.label('event_category') }}
				{{ form.render('event_category', ['class': 'form-control']) }}
			</div>
			<div class="form-group">
				{{ form.label('city') }}
				{{ form.render('city', ['class': 'form-control']) }}
			</div>
			<div class="form-group">
				{{ form.label('address') }}
				{{ form.render('address', ['class': 'form-control', 'placeholder': 'Empty']) }}<br><button id="verifyaddress" type="button" class="btn btn-warning">Retrieve location</button>
			</div>
			<div class="form-group">
				{{ form.render('latitude', ['class': 'form-control', 'placeholder': 'Empty']) }}
			</div>
			<div class="form-group">
				{{ form.render('longitude', ['class': 'form-control', 'placeholder': 'Empty']) }}
			</div>
			<div class="form-group">
				{{ form.label('time') }}
				<div class='input-group date'>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
					{{ form.render('time', ['class': 'form-control', 'id': 'datetimepicker']) }}
				</div>
			</div>
			<div class="form-group">
				{{ form.label('duration') }}
				{{ form.render('duration', ['class': 'form-control', 'placeholder': 'Days']) }}
			</div>
			<div class="control-group">
				{{ submit_button("Add event", "class": "btn btn-success") }}
				<a href="{{ url("./index/index") }}" class="btn btn-default" role="button">Cancel</a>
			</div>
		</div>
	</div>
	{{ end_form() }}
{% elseif requested_certification == 0 %}
<h3>You are not certified! If you would like to become a certified user please request certification from our administrator.</h3>
<a href="{{ url("./event/request") }}" class="btn btn-primary" role="button">Request certification</a>
{% elseif certified == 2 %}
<h3>Our administrator rejected your certification request. You are not allowed to host events.</h3>
{% else %}
<h3>Your request has been received. Please wait until our administrator approves it.</h3>
{% endif %}
