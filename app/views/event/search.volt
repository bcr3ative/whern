{{ content() }}

{{ link_to("index/index", "Back to Search", "class": "btn btn-default") }}
<br><br>

<div class='row'>
	<div class="panel panel-info">
		<div class="panel-heading">Search results</div>
		<table class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Date and time</th>
					<th>City</th>
					<th>Address</th>
					<th>Duration</th>
				</tr>
			</thead>
			<tbody>
				{%for i in search_results%}
					<tr>
						<td>{{ link_to('event/eventInfo/' ~ i.id_event, i.name) }}</td>
						<td>{{i.description}}</td>
						<td>{{i.time}}</td>
						<td>{{i.city}}</td>
						<td>{{i.address}}</td>
						<td>{{i.days_num}} days</td>
					</tr>
				{%endfor%}
			</tbody>
		</table>
	</div>
</div>