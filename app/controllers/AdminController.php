<?php

class AdminController extends ControllerBase {

    public function indexAction() {
    	$this->tag->setTitle('Admin');
        parent::initialize();
        
		$query = $this->modelsManager->createQuery('SELECT * FROM User WHERE certified = 0 AND requested_certification = 1');
		$users = $query->execute();

        $this->view->setVar("users", $users);
    }

    public function acceptAction() {
    	if(!empty($_GET['user'])) {
    		$user = User::findFirst(
				array(
					"id_user = :id_user:",
					'bind' => array(
						'id_user'    => $_GET['user']
					)
				)
			);

    		$user->certified = 1;

    		if($user->save()) {
    			return $this->forward('admin/index');
    		}
    	}
    }

    public function rejectAction() {
    	if(!empty($_GET['user'])) {
    		$user = User::findFirst(
				array(
					"id_user = :id_user:",
					'bind' => array(
						'id_user'    => $_GET['user']
					)
				)
			);

    		$user->certified = 2;

    		if($user->save()) {
    			return $this->forward('admin/index');
    		}
    	}
    }

}

