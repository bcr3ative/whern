<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder;

class ProfileController extends ControllerBase {

	public function initialize() {
        $this->tag->setTitle('Profile');
        parent::initialize();
    }

    public function indexAction() {
    	$user = User::findFirst(
			array(
				"id_user = :id_user:",
				'bind' => array(
					'id_user'    => $this->session->get('auth')['id']
				)
			)
		);

		$query = $this->modelsManager->createQuery('SELECT Event.id_event, Event.name, Event.description, Event.time, Event.city, Event.address, Event.days_num FROM Arrival INNER JOIN Event ON Arrival.id_event = Event.id_event WHERE Arrival.id_user = :id_user:');
		$arrivals = $query->execute(array('id_user' => $this->session->get('auth')['id']));

		$this->view->setVar("user", $user);
		$this->view->setVar("arrivals", $arrivals);
    }

    public function editAction() {
    	$this->view->form = new EditProfileForm;
    	if($this->request->isPost()) {
	    	if(isset($_POST['edit_profile'])) {
	    		$user = User::findFirst(
					array(
						"id_user = :id_user:",
						'bind' => array(
							'id_user'    => $this->session->get('auth')['id']
						)
					)
				);
	    		$form = new EditProfileForm;
	    		$form->bind($_POST, $user);
	    		if (!$form->isValid($_POST)) {
		            foreach ($form->getMessages() as $message) {
		                $this->flash->error($message);
		            }
		        } 

		        if($user->save()) {
		        	return $this->forward('profile/index');
		        }
	    	} 
    	}
    }
}
