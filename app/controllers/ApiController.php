<?php

use \Phalcon\Http\Response;

class ApiController extends \Phalcon\Mvc\Controller
{

	private static $HTTP_OK = 200;
	private static $HTTP_UNAUTHORIZED = 401;
	private static $HTTP_NOTFOUND = 404;

	private function createToken() {
		return md5(uniqid(rand(), true));
	}

	private function getTokenFromHeaders(array $headers) {
		$authorization = $headers["Authorization"];
    	$token = explode(" ", $authorization)[1];
    	return $token;
	}

	private function createResponse($status_code, array $data) {
		$response = new Response();
		$response->setStatusCode($status_code);
		$response->setContent(json_encode($data));
		$response->setContentType('application/json', 'UTF-8');
		return $response;
	}

    public function indexAction() {}

    public function loginAction() {
    	$this->view->disable();
    	if ($this->request->isPost()) {
    		$email = $this->request->getPost('email');
			$password = $this->request->getPost('password');
			$user = User::findFirst(
				array(
			        "email = :email:",
			        'bind' => array(
			            'email'    => $email
			        )
			    )
			);
			if (count($user) == 1) {
				if ($this->security->checkHash($password, $user->password)) {
					$token = $this->createToken();
					$content = array("auth-token" => $token);
					$user->auth_token = $token;
					$user->save();
					return $this->createResponse(ApiController::$HTTP_OK, $content);
				} else {
					return $this->createResponse(ApiController::$HTTP_UNAUTHORIZED, array());
				}
			} else {
				return $this->createResponse(ApiController::$HTTP_UNAUTHORIZED, array());
			}
    	}
    	return $this->createResponse(ApiController::$HTTP_NOTFOUND, array());
    }

    public function logoutAction() {
    	$this->view->disable();
    	if ($this->request->isPost()) {
    		$token = $this->getTokenFromHeaders(getallheaders());
			$user = User::findFirst(
				array(
			        "auth_token = :token:",
			        'bind' => array(
			            'token'    => $token
			        )
			    )
			);
			if (count($user) == 1) {
				$user->auth_token = NULL;
				$user->save();
				return $this->createResponse(ApiController::$HTTP_OK, array());
			}
			return $this->createResponse(ApiController::$HTTP_UNAUTHORIZED, array());
    	}
    	return $this->createResponse(ApiController::$HTTP_NOTFOUND, array());
    }

    public function eventAction() {
    	$this->view->disable();
		if ($this->request->isGet()) {
			$query = $this->request->getQuery();
			if (count($query) == 1) {
				$events = Event::find();
				$content = array();
				foreach ($events as $event) {
	    			$content[$event->id_event] = array(
	    				"name" => $event->name,
	    				"description" => $event->description,
	    				"time" => $event->time,
	    				"duration" => $event->days_num,
	    				"city" => $event->city,
	    				"address" => $event->$address
	    			);
	    		}
			} else if (isset($query["id"])) {
				$content = array();
				$event = Event::findFirst(
					array(
				        "id_event = :id:",
				        'bind' => array(
				            'id'    => $query["id"]
				        )
				    )
				);
				$content[$event->id_event] = array(
    				"name" => $event->name,
    				"description" => $event->description,
    				"time" => $event->time,
    				"duration" => $event->days_num,
    				"city" => $event->city,
    				"address" => $event->$address
    			);
			} else if (isset($query["name"])) {
				/*$events = Event::find(
					array(
				        "name = :name:",
				        'bind' => array(
				            'name'    => $query["name"]
				        )
				    )
				);*/
				$res = $this->modelsManager->createQuery('SELECT * FROM Event WHERE name LIKE :name:');
        		$events = $res->execute(array('name' => '%'.$query["name"].'%'));

				$content = array();
				foreach ($events as $event) {
	    			$content[$event->id_event] = array(
	    				"name" => $event->name,
	    				"description" => $event->description,
	    				"time" => $event->time,
	    				"duration" => $event->days_num,
	    				"city" => $event->city,
	    				"address" => $event->address
	    			);
	    		}
			} else if (isset($query["city"])) {
				$events = Event::find(
					array(
				        "city = :city:",
				        'bind' => array(
				            'city'    => $query["city"]
				        )
				    )
				);
				$content = array();
				foreach ($events as $event) {
	    			$content[$event->id_event] = array(
	    				"name" => $event->name,
	    				"description" => $event->description,
	    				"time" => $event->time,
	    				"duration" => $event->days_num,
	    				"city" => $event->city,
	    				"address" => $event->address
	    			);
	    		}
			}
			return $this->createResponse(ApiController::$HTTP_OK, $content);
		}
		return $this->createResponse(ApiController::$HTTP_NOTFOUND, array());
    }

    public function categoryAction() {
		$this->view->disable();
		if ($this->request->isGet()) {
			$categories = Category::find();
			$content = array();
			foreach ($categories as $category) {
    			$content[$category->id_category] = array(
    				"name" => $category->name,
    				"description" => $category->description,
    				"subcategory" => $category->subcategory
    			);
    		}
    		return $this->createResponse(ApiController::$HTTP_OK, $content);
    	}
    	return $this->createResponse(ApiController::$HTTP_NOTFOUND, array());
    }

    public function commentAction() {
    	$this->view->disable();
    	if ($this->request->isGet()) {
    		$token = $this->getTokenFromHeaders(getallheaders());
			$user = User::findFirst(
				array(
			        "auth_token = :token:",
			        'bind' => array(
			            'token'    => $token
			        )
			    )
			);
			if (count($user) == 1) {
				$query = $this->request->getQuery();
				if (count($query) == 1) {
					$comments = Comment::find(
						array(
							"id_user = :id:",
					        'bind' => array(
					            'id'    => $user->id_user
					        )
						)
					);
					$content = array();
					foreach ($comments as $comment) {
		    			$content[$comment->id_comment] = array(
		    				"event_id" => $comment->id_event,
		    				"text" => $comment->text,
		    				"time" => $comment->time
		    			);
		    		}
		    		return $this->createResponse(ApiController::$HTTP_OK, $content);
				} else if (isset($query['event_id'])) {
					$comments = Comment::find(
						array(
							"id_event = :id_event:",
					        'bind' => array(
					            'id_event' => $query['event_id']
					        )
						)
					);
					$content = array();
					foreach ($comments as $comment) {
		    			$content[$comment->id_comment] = array(
		    				"event_id" => $comment->id_event,
		    				"text" => $comment->text,
		    				"time" => $comment->time
		    			);
		    		}
		    		return $this->createResponse(ApiController::$HTTP_OK, $content);
				}
			}
			return $this->createResponse(ApiController::$HTTP_UNAUTHORIZED, array());
    	}
    	return $this->createResponse(ApiController::$HTTP_NOTFOUND, array());
    }
}

