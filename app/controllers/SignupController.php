<?php

class SignupController extends ControllerBase {

	public function initialize() {
        $this->tag->setTitle('Sign Up');
        parent::initialize();
    }

	public function indexAction() {
		$this->view->form = new SignupForm;
	}

	public function registerAction() {
		if ($this->request->isPost()) {
			$form = new SignupForm;
    		$user = new User();

	    	$data = $this->request->getPost();
	    	if (!$form->isValid($data, $user)) {
	            foreach ($form->getMessages() as $message) {
	                $this->flash->error($message);
	            }
	        }

			$email = $this->request->getPost('email');
			$password = $this->request->getPost('password');
			$nickname = $this->request->getPost('nickname');

			$user->email = $email;
			$user->nickname = $nickname;
			$user->password = $this->security->hash($password);

			if ($user->save()) {
				return $this->forward('session/index');
			}
		}
	}

}

