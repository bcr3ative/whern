<?php

class EventController extends ControllerBase {

	public function initialize() {
        $this->tag->setTitle('Event');
        parent::initialize();
    }

    public function indexAction() {
        $user = User::findFirst(
            array(
                "id_user = :id_user:",
                'bind' => array(
                    'id_user'    => $this->session->get('auth')['id']
                )
            )
        );
        $this->view->certified = $user->certified;
        $this->view->requested_certification = $user->requested_certification;
        $this->view->form = new EventForm;
    }

    public function addAction() {
    	$form = new EventForm;
    	$event = new Event();
    	$event_category = new EventCategory();

    	$data = $this->request->getPost();
    	if (!$form->isValid($data, $event)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
        }

        $event->name = $data['event_name'];
        $event->description = $data['description'];
        $event->days_num = $data['duration'];
        $event->time = $data['time'];
        $event->city = $data['city'];
        $event->address = $data['address'];
        $event->latitude = $data['latitude'];
        $event->longitude = $data['longitude'];
        $event->author = $this->session->get('auth')['id'];

        if ($event->save() == true) {
        	$id_event = $event->id_event;
        	$event_category->id_event = $id_event;
        	$event_category->id_category = $data['event_category'];
        	if($event_category->save() == false) {
        		$this->flash->error('Something went wrong');
        	} else {
        		$this->flash->success('Event added to db');
        	}
        }
    }

    public function searchAction() {
        if ($this->request->isPost()) {
            $data = $this->request->getPost();

            $event_city = $this->request->getPost('event_city');
            $category_id = $this->request->getPost('category_id');
            $event_time = $this->request->getPost('event_time');

            if ($event_city != '') {
                $query_text = "SELECT Event.id_event, Event.city, Event.name, Event.description, Event.address, Event.time, Event.days_num, EventCategory.id_category, Event.longitude, Event.latitude FROM Event INNER JOIN EventCategory ON Event.id_event = EventCategory.id_event WHERE Event.city = :event_city:";
                $query_params = array();
                $query_params['event_city'] = $event_city;

                if ($category_id != '') {
                    $query_text = "{$query_text} AND EventCategory.id_category = :category_id:";
                    $query_params['category_id'] = $category_id;

                }
                if ($event_time != '') {
                    $query_text = "{$query_text} AND Event.time > :event_time:";
                    $query_params['event_time'] = $event_time;
                }
                $query_text = "{$query_text} ORDER BY Event.time ASC";

                $query = $this->modelsManager->createQuery($query_text);
                $results = $query->execute($query_params);
                $this->view->search_results = $results;
            } else {
                $this->flash->error('You must provide at least a city to search for events.');
            }
        }
    }

    public function requestAction() {
        $user = User::findFirst(
            array(
                "id_user = :id_user:",
                'bind' => array(
                    'id_user'    => $this->session->get('auth')['id']
                )
            )
        );

        $user->requested_certification = 1;

        if($user->save()) {
           return $this->forward('event/index'); 
       }
    }

    public function myEventsAction() {
        $events = Event::find(
            array(
                "author = :author:",
                'bind' => array(
                    'author'    => $this->session->get('auth')['id']
                )
            )
        );
        $this->view->setVar("events", $events);
    }

    public function eventInfoAction($event_id) {
        $event_info = Event::findFirst(
            array(
                "id_event = :id_event:",
                'bind' => array(
                    'id_event' => $event_id
                )
            )
        );

        $query = $this->modelsManager->createQuery('SELECT User.nickname, Comment.text, Comment.time FROM Comment INNER JOIN User ON Comment.id_user = User.id_user WHERE Comment.id_event = :id_event: ORDER BY time desc');
        $comments = $query->execute(array('id_event' => $event_id));

        $query2 = $this->modelsManager->createQuery('SELECT COUNT(*) AS applied FROM Arrival WHERE Arrival.id_event = :id_event: AND Arrival.id_user = :id_user:');
        $arrival = $query2->execute(array('id_event' => $event_id, 'id_user' => $this->session->get('auth')['id']));

        $this->view->event_info = $event_info;
        $this->view->setVar("comments", $comments);
        $this->view->setVar("arrival", $arrival[0]['applied']);
        $this->view->form = new CommentForm;
    }

    public function addCommentAction() {
        $form = new CommentForm;
        $comment = new Comment();
        $comment->id_event = $this->request->getPost('id_event');
        $comment->id_user = $this->session->get('auth')['id'];
        $comment->text = $this->request->getPost('comment');

        if($comment->save()) {
            return $this->forward('event/eventInfo/'.$comment->id_event); 
        }
    }

    public function editAction($id_event) {
        $eventInfo = Event::findFirst(
            array(
                "id_event = :id_event:",
                'bind' => array(
                    'id_event' => $id_event
                )
            )
        );

        $category = EventCategory::findFirst(
            array(
                "id_event = :id_event:",
                'bind' => array(
                    'id_event' => $id_event
                )
            )
        );

        $this->view->setVar("event", $eventInfo);
        $this->view->setVar("category", $category);
        $this->view->form = new EventForm;

        if($this->request->isPost()) {
            $form = new EventForm;
            $event = $eventInfo;

            $data = $this->request->getPost();

            $event->id_event = $id_event;
            $event->name = $data['event_name'];
            $event->description = $data['description'];
            $event->days_num = $data['duration'];
            $event->time = $data['time'];
            $event->city = $data['city'];
            $event->address = $data['address'];

            if($data['latitude'] != NULL) {
                $event->latitude = $data['latitude'];
            }

            if($data['longitude'] != NULL) {
                $event->longitude = $data['longitude'];
            } 
            
            
            $event->author = $this->session->get('auth')['id'];

            if ($event->save() == true) {
                return $this->forward('event/eventInfo/'.$event->id_event);
            }

        }
    }

    public function deleteAction($id_event) {
        $query = $this->modelsManager->createQuery('DELETE FROM Event WHERE id_event = :id_event:');
        $result = $query->execute(array('id_event' => $id_event));

        if($result->success()) {
            return $this->forward('event/myEvents/');
        }

    }

    public function applyAction($id_event) {
        $arrival = new Arrival();
        $arrival->id_event = $id_event;
        $arrival->id_user = $this->session->get('auth')['id'];

        if($arrival->save()) {
            return $this->forward('event/eventInfo/'.$arrival->id_event);
        }
    }
}
