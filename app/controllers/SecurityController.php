<?php

class SecurityController extends ControllerBase
{

    public function indexAction()
    {
    	$this->view->form = new SecurityForm;	
    }

    public function changePasswordAction() {
    	if($this->request->isPost()) {
    		$user = User::findFirst(
				array(
					"id_user = :id_user:",
					'bind' => array(
						'id_user'    => $this->session->get('auth')['id']
					)
				)
			);

    		$old_password = $this->request->getPost('old_password');
    		$new_password = $this->request->getPost('new_password');
    		$repeated_password = $this->request->getPost('repeated_password');

			if($this->security->checkHash($old_password, $user->password)) {
				if($new_password == $repeated_password) {
					$user->password = $this->security->hash($new_password);
				} else {
					$this->flash->error('Passwords don\'t match');
				}
			} else {
				$this->flash->error('Wrong current password');
			}

			$form = new SecurityForm;
			if (!$form->isValid($_POST)) {
		        foreach ($form->getMessages() as $message) {
		            $this->flash->error($message);
		        }
		    } 

			if($user->save()) {
				return $this->forward('index/index');
			}

    	}
    }

}

