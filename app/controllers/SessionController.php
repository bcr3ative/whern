<?php

class SessionController extends ControllerBase {

	public function initialize() {
        $this->tag->setTitle('Log In');
        parent::initialize();
    }

    public function indexAction() {
    	$this->view->form = new LoginForm;
    }

    public function loginAction() {
    	if ($this->request->isPost()) {
    		$form = new LoginForm;
    		$user = new User();

	    	$data = $this->request->getPost();
	    	if (!$form->isValid($data, $user)) {
	            foreach ($form->getMessages() as $message) {
	                $this->flash->error($message);
	            }
	        }

			$email = $this->request->getPost('email');
			$password = $this->request->getPost('password');

			$user = User::findFirst(
				array(
			        "email = :email:",
			        'bind' => array(
			            'email'    => $email
			        )
			    )
			);

	        if ($user) {
	            if ($this->security->checkHash($password, $user->password)) {
	                // The password is valid
	                $this->session->set('auth',
						array('id'   => $user->id_user,
							  'email' => $user->email)
					);
					$this->flash->success('Welcome ' . $user->nickname);
					return $this->forward('index/index');
	            }
	        } else {
	            // To protect against timing attacks. Regardless of whether a user exists or not, the script will take roughly the same amount as it will always be computing a hash.
	            $this->security->hash(rand());
	            $this->flash->error('Wrong email/password');
	        }
			
			return $this->forward('session/index');
		}
    }

    public function logoutAction() {
		$this->session->destroy();
		$this->flash->success('Goodbye!');
        return $this->forward('index/index');
	}
}
