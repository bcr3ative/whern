<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;

class EventSearchForm extends Form {

	public function initialize() {
		$name = new Text("event_city");
		$name->setLabel("City");
		$name->addValidators(array(
            new PresenceOf(array(
                'message' => 'City of event origination is required.'
            ))
        ));
		$this->add($name);

		$category = new Select(
			"category_id",
			Category::find(),
			array(
				'useEmpty' => true,
				'emptyText'=> 'Please Select...',
				'using'    => array(
					'id_category', 'name'
				)
			)
		);
		$category->setLabel("Category");
		$this->add($category);

		$time = new Text("event_time");
		$time->setLabel("Date");
        $this->add($time);
	}
}