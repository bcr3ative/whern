<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class CommentForm extends Form {

	public function initialize() {
		$comment = new TextArea("comment");
		$comment->setLabel("Comment");
		$comment->addValidators(array(
	        new PresenceOf(array(
	            'message' => 'Comment is required'
	        ))
	    ));
		$this->add($comment);

		$event = new Hidden("id_event");
		$this->add($event);
	}

}