<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

class EditProfileForm extends Form {

	public function initialize() {

        $user = User::findFirst(
            array(
                "id_user = :id_user:",
                'bind' => array(
                    'id_user'    => $this->session->get('auth')['id']
                )
            )
        );


		$email = new Text("email");
		$email->setLabel("E-mail");
        $email->setDefault($user->email);
		$email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Email is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));
        $email->clear();
		$this->add($email);

        $nickname = new Text("nickname");
        $nickname->setLabel("Nickname");
        $nickname->setDefault($user->nickname);
        $nickname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Nickname is required'
            ))
        ));
        $this->add($nickname);

        $name = new Text("name");
        $name->setLabel("Name");
        $name->setDefault($user->name);
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Name is required'
            ))
        ));
        $this->add($name);

        $surname = new Text("surname");
        $surname->setLabel("Surname");
        $surname->setDefault($user->surname);
        $surname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Surname is required'
            ))
        ));
        $this->add($surname);
	}

}