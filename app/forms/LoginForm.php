<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class LoginForm extends Form {

	public function initialize() {
		$email = new Text("email");
		$email->setLabel("E-mail");
		$email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Email is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));
		$this->add($email);

		$password = new Password("password");
		$password->setLabel("Password");
		$password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Password is required'
            ))
        ));
		$this->add($password);
	}

}