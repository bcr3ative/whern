<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;

class SecurityForm extends Form {

	public function initialize() {

        $current_password = new Password("old_password");
        $current_password->setLabel("Current password");
        $current_password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Current password is required'
            ))
        ));
        $current_password->clear();
        $this->add($current_password);

		$new_password = new Password("new_password");
		$new_password->setLabel("New password");
		$new_password->addValidators(array(
            new PresenceOf(array(
                'message' => 'New password is required'
            ))
        ));
        $new_password->clear();
		$this->add($new_password);

        $repeated_password = new Password("repeated_password");
        $repeated_password->setLabel("Repeat password");
        $repeated_password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Please repeat new password'
            ))
        ));
        $repeated_password->clear();
        $this->add($repeated_password);

	}

}