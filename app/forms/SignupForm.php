<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

class SignupForm extends Form {

	public function initialize() {
		$email = new Text("email");
		$email->setLabel("E-mail");
		$email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Email is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));
        $email->clear();
		$this->add($email);

		$password = new Password("password");
		$password->setLabel("Password");
		$password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Password is required'
            ))
        ));
        $password->clear();
		$this->add($password);

        $nickname = new Text("nickname");
        $nickname->setLabel("Nickname");
        $nickname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Nickname is required'
            ))
        ));
        $this->add($nickname);
	}

}