<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Forms\Element\Hidden;

class EventForm extends Form {

	public function initialize() {
		$name = new Text("event_name");
		$name->setLabel("Event name");
		$name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Event name is required'
            ))
        ));
		$this->add($name);

		$description = new Text("description");
		$description->setLabel("Event description");
		$description->addValidators(array(
            new PresenceOf(array(
                'message' => 'Event description is required'
            ))
        )); 
		$this->add($description);

		$category = new Select(
			"event_category",
			Category::find(),
			array(
				'using' => array(
					'id_category', 'name'
				)
			)
		);
		$category->setLabel("Category");
		$category->addValidators(array(
            new PresenceOf(array(
                'message' => 'Category is required'
            ))
        ));
		$this->add($category);

		$duration = new Text("duration");
		$duration->setLabel("Duration");
		$duration->addValidators(array(
            new PresenceOf(array(
                'message' => 'Duration is required'
            )),
            new Numericality(array(
                'message' => 'Duration must be numeric'
            ))
        ));
		$this->add($duration);

		$time = new Text("time");
		$time->setLabel("Date");
		$time->addValidators(array(
            new PresenceOf(array(
                'message' => 'Date is required'
            ))
        ));
        $this->add($time);

        $city = new Text("city");
        $city->setLabel("City");
        $city->addValidators(array(
            new PresenceOf(array(
                'message' => 'City is required'
            ))
        ));
        $this->add($city);

        $address = new Text("address");
		$address->setLabel("Address");
		$address->addValidators(array(
            new PresenceOf(array(
                'message' => 'Address is required'
            ))
        ));
        $this->add($address);

        $latitude = new Hidden("latitude");
		$latitude->setLabel("Latitude");
		$latitude->addValidators(array(
            new PresenceOf(array(
                'message' => 'Latitude is required'
            ))
        ));
        $this->add($latitude);

        $longitude = new Hidden("longitude");
		$longitude->setLabel("Longitude");
		$longitude->addValidators(array(
            new PresenceOf(array(
                'message' => 'Longitude is required'
            ))
        ));
        $this->add($longitude);
	}
}