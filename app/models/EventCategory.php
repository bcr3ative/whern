<?php

class EventCategory extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_event;

    /**
     *
     * @var integer
     */
    public $id_category;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('id_event', 'Event', 'id_event', array('alias' => 'Event'));
        $this->belongsTo('id_category', 'Category', 'id_category', array('alias' => 'Category'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'event_category';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EventCategory[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EventCategory
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
