<?php

class Event extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_event;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $time;

    /**
     *
     * @var integer
     */
    public $days_num;

    /**
     *
     * @var double
     */
    public $latitude;

    /**
     *
     * @var double
     */
    public $longitude;

    /**
     *
     * @var integer
     */
    public $author;

    /**
     *
     * @var string
     */
    public $city;

    /**
     *
     * @var string
     */
    public $address;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id_event', 'Arrival', 'id_event', array('alias' => 'Arrival'));
        $this->hasMany('id_event', 'Comment', 'id_event', array('alias' => 'Comment'));
        $this->hasMany('id_event', 'EventCategory', 'id_event', array('alias' => 'EventCategory'));
        $this->belongsTo('author', 'User', 'id_user', array('alias' => 'User'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'event';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Event[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Event
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
