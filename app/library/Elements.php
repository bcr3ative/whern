<?php

use Phalcon\Mvc\User\Component;

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Elements extends Component {

    private $_headerMenu = array(
        'navbar-left' => array(
            array(
                'caption' => 'Home',
                'controller' => 'index',
                'action' => 'index'
            ),
            array(
                'caption' => 'Profile',
                'controller' => 'profile',
                'action' => 'index'
            ),
            array(
                'caption' => 'About',
                'controller' => 'about',
                'action' => 'index'
            )
        ),
        'navbar-right' => array(
            array(
                'caption' => 'Sign Up',
                'controller' => 'signup',
                'action' => 'index'
            ),
            array(
                'caption' => 'Log In',
                'controller' => 'session',
                'action' => 'index'
            )
        )
    );

    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
    public function getMenu() {
        $auth = $this->session->get('auth');
        if ($auth) {
            $query = $this->modelsManager->createQuery('SELECT COUNT(*) AS events_num FROM Event WHERE author = :author:');
            $events = $query->execute(array('author' => $this->session->get('auth')['id']));

            $this->_headerMenu['navbar-right'] = array(
                array(
                    'caption' => 'Become a Host',
                    'controller' => 'event',
                    'action' => 'index'
                ),
                array(
                    'caption' => 'Log Out',
                    'controller' => 'session',
                    'action' => 'logout'
                )
            );

            if($events[0]->events_num > 0) {
                array_unshift($this->_headerMenu['navbar-right'], array(
                    'caption' => 'Events I host',
                    'controller' => 'event',
                    'action' => 'myEvents'
                ));
            }

            $user = User::findFirst(
                array(
                    "id_user = :id_user:",
                    'bind' => array(
                        'id_user'    => $this->session->get('auth')['id']
                    )
                )
            );

            if($user->admin) {
                array_push($this->_headerMenu['navbar-left'], array(
                    'caption' => 'Admin',
                    'controller' => 'admin',
                    'action' => 'index'
                ));
            }            
        } else {
            unset($this->_headerMenu['navbar-left'][1]);
        }

        $controllerName = $this->view->getControllerName();
        foreach ($this->_headerMenu as $position => $menu) {
            echo '<div class="nav-collapse">';
            echo '<ul class="nav navbar-nav ', $position, '">';
            foreach ($menu as $item) {
                if ($controllerName == $item['controller']) {
                    echo '<li class="active">';
                } else {
                    echo '<li>';
                }
                echo $this->tag->linkTo($item['controller'] . '/' . $item['action'], $item['caption']);
                echo '</li>';
            }
            echo '</ul>';
            echo '</div>';
        }

    }
}
