// Utility functions for defining different date/time pickers
$(function () {
    $("#datepicker").datetimepicker({
    	format: "DD/MM/YYYY"
    });
    $("#datetimepicker").datetimepicker({
		format:"YYYY-MM-D HH:mm:ss"
	});
});

// Utility functions for geocoding with OSM
function formAddressQuery(query) {
    return "http://nominatim.openstreetmap.org/search/" + encodeURIComponent(query) + "?format=json";
}

$("#verifyaddress").click(function() {
	$.getJSON( formAddressQuery($("#address").val()) )
    .done(function( json ) {
    	$('#address').val(json[0].display_name);
    	$('#latitude').attr('value', json[0].lat);
    	$('#longitude').attr('value', json[0].lon);
    })
    .fail(function( jqxhr, textStatus, error ) {
    	var err = textStatus + ", " + error;
    	console.log( "Request Failed: " + err );
	});
});

L.mapbox.accessToken = 'pk.eyJ1IjoiYmNyM2F0aXZlIiwiYSI6ImNpa2pvcjgycTAwNG13OGxzeDEyemFqb3UifQ.tC9vcWdRA0TptKSiINTBKg';
var mapboxTiles = L.tileLayer('https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=' + L.mapbox.accessToken, {
    attribution: '<a href="http://www.mapbox.com/about/maps/" target="_blank">Terms &amp; Feedback</a>'
});

var map = L.map('map')
    .addLayer(mapboxTiles)
    .setView([42.3610, -71.0587], 15);
var marker = L.marker([$("#latitude").val(), $("#longitude").val()]).addTo(map);
map.panTo(new L.LatLng($("#latitude").val(), $("#longitude").val()));