# Whern #

aka When/Where?

Whern is a real-time local discovery event web service. Its primary focus is to provide users location based recommendations for entertaining or exclusive events.

### What is this repository for? ###

* Quick summary

This repository provides the source code for the Whern web application built as a project for our University course Development of internet applications.

* Version

Whern is currently in active development so no version nomenclature is yet defined.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* Deployment instructions

### Contribution guidelines ###

n/a

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact