-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 12, 2016 at 11:46 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `whern`
--

-- --------------------------------------------------------

--
-- Table structure for table `arrival`
--

CREATE TABLE IF NOT EXISTS `arrival` (
  `id_user` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_event`),
  KEY `fk_user_has_event_event1_idx` (`id_event`),
  KEY `fk_user_has_event_user1_idx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `arrival`
--

INSERT INTO `arrival` (`id_user`, `id_event`) VALUES
(1, 3),
(2, 3),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_category`),
  UNIQUE KEY `id_category_UNIQUE` (`id_category`),
  KEY `fk_category_category1_idx` (`subcategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_category`, `name`, `description`, `subcategory`) VALUES
(1, 'Sport', '', NULL),
(2, 'Music', '', NULL),
(3, 'Movie', '', NULL),
(4, 'Culture', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id_comment` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_comment`),
  KEY `fk_user_has_event_event2_idx` (`id_event`),
  KEY `fk_user_has_event_user2_idx` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id_comment`, `id_user`, `id_event`, `text`, `time`) VALUES
(4, 2, 3, 'Awesome movie', '2016-02-12 22:13:58'),
(5, 1, 2, 'Rijeka-Lokomotiva 2:0', '2016-02-12 22:18:14'),
(6, 3, 3, '9/10', '2016-02-12 22:36:04'),
(7, 3, 3, ':)', '2016-02-12 22:36:13');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id_event` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `days_num` int(11) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `author` int(11) NOT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_event`,`author`),
  UNIQUE KEY `id_event_UNIQUE` (`id_event`),
  KEY `fk_event_user1_idx` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id_event`, `name`, `description`, `time`, `days_num`, `latitude`, `longitude`, `author`, `city`, `address`) VALUES
(2, 'Rijeka-Lokomotiva', 'Nogometna utakmica', '2016-02-12 17:00:00', 1, 45.3404, 14.3849, 1, 'Rijeka', 'Kantrida, Rijeka, Grad Rijeka, Primorje-Gorski Kotar, Croatia'),
(3, 'Deadpool movie', 'A former Special Forces operative turned mercenary is subjected to a rogue experiment that leaves him with accelerated healing powers, adopting the alter ego Deadpool. ', '2016-02-13 19:00:00', 1, 45.3242, 14.4557, 2, 'Rijeka', 'Janka Polića Kamova, Bulevard, Rijeka, Grad Rijeka, Primorje-Gorski Kotar, 51103'),
(4, 'Ria exam', 'exam', '2016-02-23 10:00:00', 1, 45.3381, 14.4249, 1, 'Rijeka', 'Industrijska škola, 58, Vukovarska, Banderovo, Rijeka, Grad Rijeka, Primorje-Gor'),
(5, 'My event', 'cool event', '2016-02-23 23:00:00', 2, 44.8688, 13.8461, 1, 'Pula', 'Pula, Grad Pula, Istria, 52100, Croatia');

-- --------------------------------------------------------

--
-- Table structure for table `event_category`
--

CREATE TABLE IF NOT EXISTS `event_category` (
  `id_event` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id_event`,`id_category`),
  KEY `fk_event_has_category_category1_idx` (`id_category`),
  KEY `fk_event_has_category_event_idx` (`id_event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_category`
--

INSERT INTO `event_category` (`id_event`, `id_category`) VALUES
(2, 1),
(5, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `auth_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `certified` int(11) NOT NULL DEFAULT '0',
  `requested_certification` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `id_user_UNIQUE` (`id_user`),
  UNIQUE KEY `e-mail_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `auth_token`, `email`, `password`, `nickname`, `admin`, `certified`, `requested_certification`, `name`, `surname`) VALUES
(1, NULL, 'ipetrovic@riteh.hr', '$2a$08$AKENIGwIu6I40Dmiql6WvOGOvUBakHhfu0yCW64j.1ncxQ0S6pJXe', 'Ivče', 1, 1, 0, 'Iva', 'Petrović'),
(2, NULL, 'paolo.perkovic@gmail.com', '$2a$08$XQMzZunKZxIOZySrLvC5Y.1XMTYS16sVAQNLV0RX35A2RvWro7pTa', 'Paolo', 0, 1, 1, 'Paolo', 'Perković'),
(3, NULL, 'user@user.com', '$2a$08$T1fvaE4y2JJQTszkzgYDguzF.Kco3wnHN16HF6pgfEtZXbwEWWngC', 'user', 0, 0, 1, 'User', 'User'),
(4, NULL, 'pero@pero.com', '$2a$08$tEKTEuyQ4HmleRKUWlfyQOtmmwIgCCI0DYbsomVuxWY8QaF6YlsTG', 'pero', 0, 2, 1, 'Pero', 'Perić');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `arrival`
--
ALTER TABLE `arrival`
  ADD CONSTRAINT `fk_user_has_event_event1` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_event_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `fk_category_category1` FOREIGN KEY (`subcategory`) REFERENCES `category` (`id_category`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_user_has_event_event2` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_event_user2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_event_user1` FOREIGN KEY (`author`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `event_category`
--
ALTER TABLE `event_category`
  ADD CONSTRAINT `fk_event_has_category_category1` FOREIGN KEY (`id_category`) REFERENCES `category` (`id_category`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_event_has_category_event` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
